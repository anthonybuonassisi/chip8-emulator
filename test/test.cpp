/* Unit test includes */
#define CATCH_CONFIG_MAIN
#include "test/catch.h"

/* Emulator includes */
#include "main/globals.h"
#include "main/chip8.h"
#include "main/opcodes.h"

chip8 emulator;

TEST_CASE("Opcode 0x1NNN") {
	opcode = 0x1123;
	pc = 0;
	emulator.op.op_1NNN();

	REQUIRE(pc == 0x123);
}

TEST_CASE("Opcode 0x2NNN") {
	opcode = 0x2123;
	sp = 5;
	pc = 0x100;
	uint16_t pc_prev = pc;
	emulator.op.op_2NNN();

	REQUIRE(pc == 0x123);
	REQUIRE(stack[sp - 1] == pc_prev);
}

TEST_CASE("Opcode 0x3XNN") {
	opcode = 0x3AFF;
	V[0xA] = 0xFF;
	pc = 0;
	emulator.op.op_3XNN();

	REQUIRE(pc == 4);

	V[0xA] = 0xFE;
	emulator.op.op_3XNN();

	REQUIRE(pc == 6);
}

TEST_CASE("Opcode 0x4XNN") {
	opcode = 0x4AFF;
	V[0xA] = 0xFF;
	pc = 0;
	emulator.op.op_4XNN();

	REQUIRE(pc == 2);

	V[0xA] = 0xFE;
	emulator.op.op_4XNN();

	REQUIRE(pc == 6);
}

TEST_CASE("Opcode 0x5XY0") {
	opcode = 0x5010;
	V[0] = 0xAA;
	V[1] = 0xAA;
	pc = 0;
	emulator.op.op_5XY0();

	REQUIRE(pc == 4);

	V[1] = 0xBB;
	emulator.op.op_5XY0();

	REQUIRE(pc == 6);
}

TEST_CASE("Opcode 0x6XNN") {
	opcode = 0x65DD;
	V[5] = 0;
	pc = 0;
	emulator.op.op_6XNN();

	REQUIRE(pc == 2);
	REQUIRE(V[5] == 0xDD);
}

TEST_CASE("Opcode 0x7XNN") {
	opcode = 0x75DD;
	V[5] = 1;
	pc = 0;
	emulator.op.op_7XNN();

	REQUIRE(pc == 2);
	REQUIRE(V[5] == 0xDE);
}

TEST_CASE("Opcode 0x8XY0") {
	opcode = 0x8010;
	V[0] = 1;
	V[1] = 0;
	pc = 0;
	emulator.op.op_8XY0();

    REQUIRE(pc == 2);
	REQUIRE(V[0] == V[1]);
}

TEST_CASE("Opcode 0x8XY1") {
	opcode = 0x8011;
	V[0] = 0;
	V[1] = 0;
	V[2] = 1;
	V[3] = 1;
	pc = 0;
	emulator.op.op_8XY1();
	REQUIRE(pc == 2);
	REQUIRE(V[0] == 0);

	V[0] = 0;
	V[1] = 0;
	V[2] = 1;
	V[3] = 1;
	opcode = 0x8211;
	emulator.op.op_8XY1();
	REQUIRE(pc == 4);
	REQUIRE(V[2] == 1);

	V[0] = 0;
	V[1] = 0;
	V[2] = 1;
	V[3] = 1;
	opcode = 0x8121;
	emulator.op.op_8XY1();
	REQUIRE(pc == 6);
	REQUIRE(V[1] == 1);

	V[0] = 0;
	V[1] = 0;
	V[2] = 1;
	V[3] = 1;
	opcode = 0x8321;
	emulator.op.op_8XY1();
	REQUIRE(pc == 8);
	REQUIRE(V[3] == 1);
}

TEST_CASE("Opcode 0x8XY2") {
	opcode = 0x8012;
	V[0] = 0;
	V[1] = 0;
	V[2] = 1;
	V[3] = 1;
	pc = 0;
	emulator.op.op_8XY2();
	REQUIRE(pc == 2);
	REQUIRE(V[0] == 0);

	V[0] = 0;
	V[1] = 0;
	V[2] = 1;
	V[3] = 1;
	opcode = 0x8212;
	emulator.op.op_8XY2();
	REQUIRE(pc == 4);
	REQUIRE(V[2] == 0);

	V[0] = 0;
	V[1] = 0;
	V[2] = 1;
	V[3] = 1;
	opcode = 0x8122;
	emulator.op.op_8XY2();
	REQUIRE(pc == 6);
	REQUIRE(V[1] == 0);

	V[0] = 0;
	V[1] = 0;
	V[2] = 1;
	V[3] = 1;
	opcode = 0x8322;
	emulator.op.op_8XY2();
	REQUIRE(pc == 8);
	REQUIRE(V[3] == 1);
}

TEST_CASE("Opcode 0x8XY3") {
	opcode = 0x8013;
	V[0] = 0;
	V[1] = 0;
	V[2] = 1;
	V[3] = 1;
	pc = 0;
	emulator.op.op_8XY3();
	REQUIRE(pc == 2);
	REQUIRE(V[0] == 0);

	V[0] = 0;
	V[1] = 0;
	V[2] = 1;
	V[3] = 1;
	opcode = 0x8212;
	emulator.op.op_8XY3();
	REQUIRE(pc == 4);
	REQUIRE(V[2] == 1);

	V[0] = 0;
	V[1] = 0;
	V[2] = 1;
	V[3] = 1;
	opcode = 0x8122;
	emulator.op.op_8XY3();
	REQUIRE(pc == 6);
	REQUIRE(V[1] == 1);

	V[0] = 0;
	V[1] = 0;
	V[2] = 1;
	V[3] = 1;
	opcode = 0x8322;
	emulator.op.op_8XY3();
	REQUIRE(pc == 8);
	REQUIRE(V[3] == 0);
}

TEST_CASE("Opcode 0xEX9E") {
	opcode = 0xE09E;
	key[0] = 0;
	pc = 0;
	emulator.op.op_EX9E();

	REQUIRE(pc == 2);

	key[0] = 1;
	emulator.op.op_EX9E();

	REQUIRE(pc == 6);
}

TEST_CASE("Opcode 0xEXA1") {
	opcode = 0xE0A1;
	key[0] = 0;
	pc = 0;
	emulator.op.op_EXA1();

	REQUIRE(pc == 4);

	key[0] = 1;
	emulator.op.op_EXA1();

	REQUIRE(pc == 6);
}

