#include "CImg.h"
#include <iostream>
using namespace cimg_library;

int main()
{
    // create image size 500x400x1,3 channels (RGB) default colour white
    CImg<unsigned char>  theImage(500,400,1,3,1);

    // Now change one pixel value
    theImage(10,10,1) = 255; // different colour on pixel 10, 10- green to max


    CImgDisplay main_disp(theImage); // display it
    std::cin.ignore(); //wait
}

