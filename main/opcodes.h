#pragma once

class opcodes {
  public:
	opcodes();
    void op_0NNN(void);
    void op_00E0(void);
    void op_00EE(void);
    void op_1NNN(void);
    void op_2NNN(void);
    void op_3XNN(void);
    void op_4XNN(void);
    void op_5XY0(void);
    void op_6XNN(void);
    void op_7XNN(void);
    void op_8XY0(void);
    void op_8XY1(void);
    void op_8XY2(void);
    void op_8XY3(void);
    void op_8XY4(void);
    void op_8XY5(void);
    void op_8XY6(void);
    void op_8XY7(void);
    void op_8XYE(void);
    void op_9XY0(void);
    void op_ANNN(void);
    void op_BNNN(void);
    void op_CXNN(void);
    void op_DXYN(void);
    void op_EX9E(void);
    void op_EXA1(void);
    void op_FX07(void);
    void op_FX0A(void);
    void op_FX15(void);
    void op_FX18(void);
    void op_FX1E(void);
    void op_FX29(void);
    void op_FX33(void);
    void op_FX55(void);
    void op_FX65(void);
    void error(void);
};

