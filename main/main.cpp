#include <iostream>
#include "main/chip8.h"

int main(void) {
	chip8 emulator;
	emulator.init();

	std::cout << "Nothing implemented in main()" << std::endl;
    return 0;
}

