#include <time.h>
#include <iostream>
#include "main/opcodes.h"
#include "main/globals.h"

opcodes::opcodes() {
	srand(time(NULL));
}

void opcodes::error() {
    std::cout << "Opcode error" << std::endl;
}

void opcodes::op_0NNN() {}
void opcodes::op_00E0() {}

void opcodes::op_00EE() {
    sp--;
    pc = stack[sp];
}

void opcodes::op_1NNN() {
    pc = opcode & 0x0FFF;
}

void opcodes::op_2NNN() {
    stack[sp] = pc;
    sp++;
    pc = opcode & 0x0FFF;
}

void opcodes::op_3XNN() {
    if (V[(opcode & 0x0F00) >> 8] == (opcode & 0x00FF))
        pc += 4;
    else
        pc += 2;
}

void opcodes::op_4XNN() {
    if (V[(opcode & 0x0F00) >> 8] != (opcode & 0x00FF))
        pc += 4;
    else
        pc += 2;
}

void opcodes::op_5XY0() {
    if (V[(opcode & 0x0F00) >> 8] == V[(opcode & 0x00F0) >> 4])
        pc += 4;
    else
        pc += 2;
}

void opcodes::op_6XNN() {
    V[(opcode & 0x0F00) >> 8] = (opcode & 0x00FF);
    pc += 2;
}

void opcodes::op_7XNN() {
    V[(opcode & 0x0F00) >> 8] += (opcode & 0x00FF);
    pc += 2;
}

void opcodes::op_8XY0() {
    V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x00F0) >> 4];
    pc += 2;
}

void opcodes::op_8XY1() {
    V[(opcode & 0x0F00) >> 8] = ((V[(opcode & 0x0F00) >> 8]) | (V[(opcode & 0x00F0) >> 4]));
    pc += 2;
}

void opcodes::op_8XY2() {
    V[(opcode & 0x0F00) >> 8] = ((V[(opcode & 0x0F00) >> 8]) & (V[(opcode & 0x00F0) >> 4]));
    pc += 2;
}

void opcodes::op_8XY3() {
    V[(opcode & 0x0F00) >> 8] = ((V[(opcode & 0x0F00) >> 8]) ^ (V[(opcode & 0x00F0) >> 4]));
    pc += 2;
}

void opcodes::op_8XY4() {
    if (V[(opcode & 0x00F0) >> 4] > (0xFF - V[(opcode & 0x0F00) >> 8])) {
        V[0xF] = 1; // carry
    } else {
        V[0xF] = 0; // no carry
    }
    V[(opcode & 0x0F00) >> 8] += V[(opcode & 0x00F0) >> 4];
    pc += 2;
}

void opcodes::op_8XY5(void) {}
void opcodes::op_8XY6(void) {}
void opcodes::op_8XY7(void) {}
void opcodes::op_8XYE(void) {}

void opcodes::op_9XY0(void) {
    if (V[(opcode & 0x0F00) >> 8] != V[(opcode & 0x00F0) >> 4])
        pc += 4;
    else
        pc += 2;
}

void opcodes::op_ANNN(void) {
	I = (opcode & 0x0FFF);
	pc += 2;
}

void opcodes::op_BNNN(void) {
	pc = (V[0] + (opcode & 0x0FFF));
}

void opcodes::op_CXNN(void) {
	uint8_t random = rand() % 255;
	V[(opcode & 0x0F00) >> 8] = (random & (opcode & 0x00FF));
}

void opcodes::op_DXYN(void) {}

void opcodes::op_EX9E(void) {
	if (key[(opcode & 0x0F00) >> 8] == 1) {
		pc += 4;
	} else {
		pc += 2;
	}
}

void opcodes::op_EXA1(void) {
	if (key[(opcode & 0x0F00) >> 8] != 1) {
		pc += 4;
	} else {
		pc += 2;
	}
}

void opcodes::op_FX07(void) {}

void opcodes::op_FX0A(void) {}

void opcodes::op_FX15(void) {}

void opcodes::op_FX18(void) {}

void opcodes::op_FX1E(void) {}

void opcodes::op_FX29(void) {}

void opcodes::op_FX33(void) {
    memory[I]     = V[(opcode & 0x0F00) >> 8] / 100;
    memory[I + 1] = (V[(opcode & 0x0F00) >> 8] / 10) % 10;
    memory[I + 2] = (V[(opcode & 0x0F00) >> 8] % 100) % 10;
    pc += 2;
}

void opcodes::op_FX55(void) {
    for (int i = 0; i <= ((opcode & 0x0F00) >> 8); i++) {
        memory[I + i] = V[(opcode & 0x0F00) >> 8];
    }
    pc += 2;
}

void opcodes::op_FX65(void) {
    for (int i = 0; i <= ((opcode & 0x0F00) >> 8); i++) {
        V[(opcode & 0x0F00) >> 8] = memory[I + i];
    }
    pc += 2;
}
