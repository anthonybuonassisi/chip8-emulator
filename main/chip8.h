#pragma once

#include "stdint.h"
#include "opcodes.h"

class chip8 {
  public:
    void init(void);
    void cycle(void);
    void decode(uint16_t);
    opcodes op;
};


