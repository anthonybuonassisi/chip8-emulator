#include <cstdint>
#include <iostream>
#include "main/chip8.h"

uint16_t opcode;
uint8_t memory[4096];
uint8_t V[16];
uint16_t I;
uint16_t pc;
uint8_t gfx[64 * 32];
uint8_t delay_timer;
uint8_t sound_timer;
uint16_t stack[16];
uint16_t sp;
uint8_t key[16];

void chip8::init() {
    pc = 0x200;
    opcode = 0;
    I = 0;
    sp = 0;
}

void chip8::decode(uint16_t opcode) {
    switch (opcode & 0xF000) {
        case 0x0000:
            switch (opcode & 0x00FF) {
                case 0x00E0:
                    op.op_00E0();
                    break;
                case 0x00EE:
                    op.op_00EE();
                    break;
                default:
                    op.op_0NNN();
            }
            break;
        case 0x1000:
            op.op_1NNN();
            break;
        case 0x2000:
            op.op_2NNN();
            break;
        case 0x3000:
            op.op_3XNN();
            break;
        case 0x4000:
            op.op_4XNN();
            break;
        case 0x5000:
            op.op_5XY0();
            break;
        case 0x6000:
            op.op_6XNN();
            break;
        case 0x7000:
            op.op_7XNN();
        case 0x8000:
            switch (opcode & 0x000F) {
                case 0x0000:
                    op.op_8XY0();
                    break;
                case 0x0001:
                    op.op_8XY1();
                    break;
                case 0x0002:
                    op.op_8XY2();
                    break;
                case 0x0003:
                    op.op_8XY3();
                    break;
                case 0x0004:
                    op.op_8XY4();
                    break;
                case 0x0005:
                    op.op_8XY5();
                    break;
                case 0x0006:
                    op.op_8XY6();
                    break;
                case 0x0007:
                    op.op_8XY7();
                    break;
                case 0x000E:
                    op.op_8XYE();
                    break;
                default:
                    op.error();
            }
            break;
        case 0xF000:
            switch (opcode & 0x00FF) {
                case 0x0033:
                    op.op_FX33();
                    break;
                default:
                    op.error();
            }
            break;
        default:
            op.error();
    }
}

void chip8::cycle() {
    // fetch
    opcode = memory[pc] << 8 | memory[pc + 1];

    // decode and execute
    decode(opcode);
}
