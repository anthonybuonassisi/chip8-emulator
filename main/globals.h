#pragma once

#include <cstdint>

extern uint16_t opcode;
extern uint8_t memory[4096];
extern uint8_t V[16];
extern uint16_t I;
extern uint16_t pc;
extern uint8_t gfx[64 * 32];
extern uint8_t delay_timer;
extern uint8_t sound_timer;
extern uint16_t stack[16];
extern uint16_t sp;
extern uint8_t key[16];


