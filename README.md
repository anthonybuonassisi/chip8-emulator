# README #

This project is a chip8 emulator.

### Build ###

Build and run the main app
`bazel build //main:main && bazel-bin/main/main`

Build and run tests
`bazel build //test:test && bazel-bin/test/test`

